export const shopNameList: Array<string> = [
	'unknown',
	'flying',
	'forge',
	'magic',
	'cursed', // picture missing
	'fruity',
	'razad',
	'souk',
	'neerhel',
	'barbarian',
	'secret',
	'elit', // original game has no picture
	'chens',
	'merchant',
	'merchant',
	'merchant',
	'merchant',
	'merchant',
	'merchant',
	'merchant',
	'filou'
];

export const itinerantShopNameList: Array<string> = [
	'unknown',
	'merchant_monday',
	'merchant_tuesday',
	'merchant_wednesday',
	'merchant_thursday',
	'merchant_friday',
	'merchant_saturday',
	'merchant_sunday'
];
