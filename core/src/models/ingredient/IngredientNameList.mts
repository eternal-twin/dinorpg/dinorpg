export const ingredientNameList: Record<number, string | undefined> = {
	1: 'merou_lujidane',
	2: 'poisson_vengeur',
	3: 'an_guili_guilille',
	4: 'globulos',
	5: 'super_poisson',
	6: 'touffe_de_fourrure',
	7: 'roche_radio_active',
	8: 'griffes_acerees',
	9: 'corne_en_chocolat',
	10: 'oeil_visqueux',
	11: 'langue_monstrueuse',
	12: 'energie_foudre',
	13: 'energie_air',
	14: 'energie_eau',
	15: 'energie_feu',
	16: 'energie_bois',
	17: 'silex_taille',
	18: 'fragment_de_texte_ancien',
	19: 'vieil_anneau_precieux',
	20: 'calice_cisele',
	21: 'collier_karat',
	22: 'broche_en_parfait_etat',
	23: 'superbe_couronne_royale',
	24: 'feuilles_de_pelinae',
	25: 'bolet_phalisk_blanc',
	26: 'orchidee_fantasque',
	27: 'racine_de_figonicia',
	28: 'sadiquae_mordicus',
	29: 'flaureole',
	30: 'spore_etheral',
	31: 'pousse_sombre',
	32: 'graine_de_devoreuse',
	33: 'dent_de_dorogon',
	34: 'bras_mecanique'
};
