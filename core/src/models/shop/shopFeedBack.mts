export interface ShopFeedBack {
	itemId: number;
	quantity: number;
	gold?: number;
}
