import { DinozRace } from '../dinoz/DinozRace.mjs';

export interface DinozShopFicheLite {
	id: string;
	display: string;
	race: string;
}
