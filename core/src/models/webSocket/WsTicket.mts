export interface WsTicket {
	uuid: string;
	channel: string;
	userAgent: string;
	ipAddress: string;
	playerId: string;
	timestamp: number;
}
