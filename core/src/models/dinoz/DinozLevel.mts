export const levelList = [
	{
		id: 1,
		experience: 100
	},
	{
		id: 2,
		experience: 107
	},
	{
		id: 3,
		experience: 115
	},
	{
		id: 4,
		experience: 124
	},
	{
		id: 5,
		experience: 133
	},
	{
		id: 6,
		experience: 143
	},
	{
		id: 7,
		experience: 154
	},
	{
		id: 8,
		experience: 165
	},
	{
		id: 9,
		experience: 178
	},
	{
		id: 10,
		experience: 191
	},
	{
		id: 11,
		experience: 206
	},
	{
		id: 12,
		experience: 221
	},
	{
		id: 13,
		experience: 238
	},
	{
		id: 14,
		experience: 256
	},
	{
		id: 15,
		experience: 275
	},
	{
		id: 16,
		experience: 295
	},
	{
		id: 17,
		experience: 318
	},
	{
		id: 18,
		experience: 341
	},
	{
		id: 19,
		experience: 367
	},
	{
		id: 20,
		experience: 395
	},
	{
		id: 21,
		experience: 424
	},
	{
		id: 22,
		experience: 456
	},
	{
		id: 23,
		experience: 490
	},
	{
		id: 24,
		experience: 527
	},
	{
		id: 25,
		experience: 567
	},
	{
		id: 26,
		experience: 596
	},
	{
		id: 27,
		experience: 655
	},
	{
		id: 28,
		experience: 704
	},
	{
		id: 29,
		experience: 757
	},
	{
		id: 30,
		experience: 814
	},
	{
		id: 31,
		experience: 875
	},
	{
		id: 32,
		experience: 941
	},
	{
		id: 33,
		experience: 1011
	},
	{
		id: 34,
		experience: 1087
	},
	{
		id: 35,
		experience: 1169
	},
	{
		id: 36,
		experience: 1256
	},
	{
		id: 37,
		experience: 1351
	},
	{
		id: 38,
		experience: 1452
	},
	{
		id: 39,
		experience: 1561
	},
	{
		id: 40,
		experience: 1678
	},
	{
		id: 41,
		experience: 1804
	},
	{
		id: 42,
		experience: 1936
	},
	{
		id: 43,
		experience: 2085
	},
	{
		id: 44,
		experience: 2242
	},
	{
		id: 45,
		experience: 2409
	},
	{
		id: 46,
		experience: 2591
	},
	{
		id: 47,
		experience: 2785
	},
	{
		id: 48,
		experience: 2993
	},
	{
		id: 49,
		experience: 3218
	},
	{
		id: 50,
		experience: 3444
	},
	{
		id: 51,
		experience: 3459
	},
	{
		id: 52,
		experience: 3718
	},
	{
		id: 53,
		experience: 3997
	},
	{
		id: 54,
		experience: 4297
	},
	{
		id: 55,
		experience: 4620
	},
	{
		id: 56,
		experience: 5339
	},
	{
		id: 57,
		experience: 5739
	},
	{
		id: 58,
		experience: 6169
	},
	{
		id: 59,
		experience: 6632
	},
	{
		id: 60,
		experience: 7130
	},
	{
		id: 61,
		experience: 7664
	},
	{
		id: 62,
		experience: 8239
	},
	{
		id: 63,
		experience: 8857
	},
	{
		id: 64,
		experience: 9522
	},
	{
		id: 65,
		experience: 10236
	},
	{
		id: 66,
		experience: 11003
	},
	{
		id: 67,
		experience: 11829
	},
	{
		id: 68,
		experience: 12716
	},
	{
		id: 69,
		experience: 13670
	},
	{
		id: 70,
		experience: 14695
	},
	{
		id: 71,
		experience: 15797
	},
	{
		id: 72,
		experience: 16982
	},
	{
		id: 73,
		experience: 18256
	},
	{
		id: 74,
		experience: 19625
	},
	{
		id: 75,
		experience: 21097
	},
	{
		id: 76,
		experience: 22679
	},
	{
		id: 77,
		experience: 24380
	},
	{
		id: 78,
		experience: 26209
	},
	{
		id: 79,
		experience: 28174
	},
	// ???
	{
		id: 80,
		experience: 0
	}
];
