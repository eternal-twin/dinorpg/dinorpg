export type PlayerStats = {
	stat: string;
	quantity: number;
};
