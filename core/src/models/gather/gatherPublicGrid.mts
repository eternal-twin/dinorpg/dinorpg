export type GatherPublicGrid = {
	grid: (-1 | 0)[][];
	gatherTurn: number;
	gatherType: string;
};
