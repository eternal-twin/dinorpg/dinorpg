export enum FightType {
	MONSTER_FIGHT = 1,
	DINOZ_FIGHT = 2,
	DOJO_FIGHT = 3,
	CASTLE_FIGHT = 4
}
