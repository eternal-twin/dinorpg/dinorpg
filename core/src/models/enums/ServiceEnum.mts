export enum ServiceEnum {
	CONCENTRATION = 'concentration',
	DINOZ = 'dinoz',
	REFRESH_DINOZLIST = 'refreshDinozList',
	FIGHT = 'fight'
}
