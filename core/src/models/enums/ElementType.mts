export enum ElementType {
	FIRE = 1,
	WOOD = 2,
	WATER = 3,
	LIGHTNING = 4,
	AIR = 5,
	VOID = 6
}

export const ElementNames = ['', 'fire', 'wood', 'water', 'lightning', 'air', 'void'];
