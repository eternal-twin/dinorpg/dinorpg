export interface SiteStat {
	name: string;
	score: number;
}
