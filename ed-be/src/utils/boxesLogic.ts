import { getBoxHandlerInformations } from '../dao/playerDao.js';
import { ExpectedError } from '@drpg/core/utils/ExpectedError';
import gameConfig from '../config/game.config.js';
import { Item, itemList } from '@drpg/core/models/item/ItemList';
import { ItemFiche } from '@drpg/core/models/item/ItemFiche';
import { boxProbabilities } from '@drpg/core/models/item/itemProbability';
import weightedRandom from './fight/weightedRandom.js';

/**
 * Calculate a player completion, an approximate score of its progress in the game and its content
 * The completion score depends on the epic rewards of the player, the average level of its active dinoz and
 * their average mission completion rate.
 * @param playerId {number} id of the player
 * @returns Completion score
 */
export async function calculatePlayerCompletion(playerId: string) {
	const boxInfo = await getBoxHandlerInformations(playerId);
	if (!boxInfo) throw new ExpectedError(`Player doesn't exist`);
	const dinozCount = boxInfo._count.dinoz;
	if (dinozCount <= 0) return dinozCount;
	const missionTotal = boxInfo.dinoz.reduce((partialSum, a) => partialSum + a._count.missions, 0);
	const AVAILABLE_MISSIONS = 55;
	const dinozLevelTotal = boxInfo.dinoz.reduce((partialSum, a) => partialSum + a.level, 0);
	const totalRewards = boxInfo.rewards.filter(r => r.rewardId <= 24).length;
	const AVAILABLE_REWARDS = 23;
	const universalCount =
		(boxInfo.cooker ? 1 : 0) +
		(boxInfo.engineer ? 1 : 0) +
		(boxInfo.matelasseur ? 1 : 0) +
		(boxInfo.merchant ? 1 : 0) +
		(boxInfo.messie ? 1 : 0) +
		(boxInfo.leader ? 1 : 0) +
		(boxInfo.priest ? 1 : 0) +
		(boxInfo.shopKeeper ? 1 : 0) +
		(boxInfo.teacher ? 1 : 0);
	const AVAILABLE_UNIVERSAL = 9;

	const coefficients = {
		dinoz: 1,
		universal: 1,
		missions: 5,
		level: 3,
		rewards: 10
	};
	const completion =
		(((dinozCount / gameConfig.dinoz.maxQuantity) * coefficients.dinoz +
			(universalCount / AVAILABLE_UNIVERSAL) * coefficients.universal +
			(missionTotal / (AVAILABLE_MISSIONS * gameConfig.dinoz.maxQuantity)) * coefficients.missions +
			(dinozLevelTotal / (gameConfig.dinoz.maxLevel * gameConfig.dinoz.maxQuantity)) * coefficients.level +
			(totalRewards / AVAILABLE_REWARDS) * coefficients.rewards) /
			(coefficients.dinoz +
				coefficients.universal +
				coefficients.missions +
				coefficients.level +
				coefficients.rewards)) *
		100;

	return completion;
}

/**
 * Roll and pick a random box.
 * Every 10th of completion, an extra roll is made. The roll with the highest tier is returned.
 * @param completion {number} completion of the player
 * @returns Box type
 */
export function selectBox(completion: number) {
	const boxOdds = [
		{ tier: 1, type: Item.BOX_COMMON, odds: 900 },
		{ tier: 2, type: Item.BOX_RARE, odds: 75 },
		{ tier: 3, type: Item.BOX_EPIC, odds: 24 },
		{ tier: 4, type: Item.BOX_LEGENDARY, odds: 1 }
	];
	const totalOdds = boxOdds.reduce((acc, box) => acc + box.odds, 0);
	const maxRolls = Math.floor(completion / 10) + 1;
	let reward = boxOdds[0]; // Pick common box as initial reward.
	let currentRoll = 0;
	while (currentRoll < maxRolls) {
		currentRoll++;
		const boxRoll = weightedRandom(boxOdds, totalOdds);
		// Use the rank to make this ID-agnostic
		if (boxRoll.tier > reward.tier) {
			reward = boxRoll;
		}
	}
	return itemList[reward.type];
}

export function boxOpening(box: ItemFiche) {
	const myBox = Object.values(itemList).find(i => i.itemId === box.itemId);
	const myProba = boxProbabilities.find(b => b.boxType === myBox?.name);
	if (!myProba) {
		throw new ExpectedError(`Special item ${box.itemId} is not implemented`);
	}

	const totalOdds = myProba.items.reduce((acc, box) => acc + box.odds, 0);
	const myItem = weightedRandom(myProba.items, totalOdds);

	return { item: myItem.item, quantity: myItem.quantity };
}
