/*
  Warnings:

  - You are about to drop the column `banner_url` on the `Clan` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Clan" DROP COLUMN "banner_url";
